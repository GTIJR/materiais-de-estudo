# Cursos

## SuperFullStack

Curso acessível no arquivo Contas GTi no drive, na seção OneBitCode

### Projetos

- OneBitExchange
  - Sistema para a conversão de moedas extrangeiras, utilizando uma API externa.
  - Tecnologias: docker, rest-client, yarn
- OneBitBot
  - Chatbot do telegram
  - Tecnologias: docker, sinatra, dialogflow
- Amigo secreto
  - Tecnologias: docker, sidekiq, ActiveJobs, Redis
- Sistema Administrativo
  - Faz email marketing
  - Tecnologias: docker, gem FAE, jobs, sidekiq
- Tinder
  - Aplicativo de encontros com geolocalização
  - Tecnologias: docker, VueJS, PWA, PostGIS, Rails API
- Slack
  - Aplicativo de conversa em salas
  - Tecnologias: ActionCable, Jobs
- Dev.OneBitCode
  - Criador de portifolios com integração de pagamento da Wirecard (Moip)
  - Tecnologias: VueJS, Cucumber, Redis, Capistrano
- Instagram
  - Tecnologias: Ionic
- OneBitForms
  - Clone do google forms
  - Tecnologias: Angular, Rails API
- OneBitTweet
  - Clone do twitter
  - Tecnologias: Rails API, React SPA, JWT, elasticSearch, Redis

### Mini Cursos
- Mini curso de Rspec
- Mini curso Rails API
- OneBitFlix
  - Clone do netflix com streaming de vídeo


## Udemy
Conta da GTi na Udemy (entrar logando com a conta google da GTi)
### Cursos
- React Native
- Curso React.js Ninja
- Gestão de Projetos - Avançado
- Preparação PMBOK (gerenciamento de projetos)

# Livros
- Clean Code
- Clean Architecture
- Refactoring
- Domain Driven Design

# Referências top

- Mostly Adequate Guide to Functional Programming
  - https://mostly-adequate.gitbooks.io/mostly-adequate-guide/
- BetterSpecs
  - http://www.betterspecs.org/
- Ruby Style Guide (referência do rubocop)
  - https://rubystyle.guide/
- Rails Style Guide (referência do rubocop)
  - https://rails.rubystyle.guide/
- Documentação oficial do Ruby
  - https://ruby-doc.org/
- Rails Guides
  - https://guides.rubyonrails.org/

# Palestrinhas

- Ruby Midwest 2011 - Keynote: Architecture the Lost Years by Robert Martin
  - https://www.youtube.com/watch?v=WpkDN78P884
- O futuro da comunidade Ruby - Ruby on Rails - Por Rafael França - 30º GURU-CE
  - https://www.youtube.com/watch?v=S_-5smCMmbA
- Escrevendo testes melhores com Rspec - Ruby - Por Heitor Miranda - 30º GURU-CE
  - https://www.youtube.com/watch?v=cHAnlE3kwBM
- Implementando o design pattern Form Objects - Ruby - Rails - Por Marcelo Veloso - 30º GURU-CE
  - https://www.youtube.com/watch?v=3T8NlRRv9mQ
- De service objects a use cases por Rodrigo Serradura
  - https://www.youtube.com/watch?v=ySNzVfmYy5g
- [Rails Architects] This time it will be different - how to properly start your next Rails app
  - https://www.youtube.com/watch?v=kURp3CE-FvM


# Canais com mais palestrinhas

- Confreaks (Rubyconfs mundiais)
  - https://www.youtube.com/c/Confreaks/videos
- Guru SP (Grupo de Usuários de Ruby de São Paulo)
  - https://www.youtube.com/user/GuruSPtalks
- Guru CE (Grupo de Usuários de Ruby do Ceará)
  - https://www.youtube.com/channel/UCb37lKRVAErU2iZeHSZNSNw
